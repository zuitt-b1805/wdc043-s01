package com.zuitt.activity;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = scanner.nextLine();
        System.out.println("Last Name:");
        String lastName = scanner.nextLine();

        System.out.println("First Subject Grade:");
        double firstSubject = scanner.nextDouble();
        System.out.println("Second Subject Grade:");
        double secondSubject = scanner.nextDouble();
        System.out.println("Third Subject Grade:");
        double thirdSubject = scanner.nextDouble();

        double average = (firstSubject + secondSubject + thirdSubject) / 3;
        String goodDay = String.format("Good day, %s %s.", firstName, lastName);
        String printGrades = String.format("Your grade average is %s", average);
        System.out.println(goodDay);
        System.out.println(printGrades);
    }
}
